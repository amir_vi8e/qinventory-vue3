const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/",
        name: "Dashboard",
        component: () => import("pages/Index.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/incoming/goods/receive/",
        name: "Incoming Goods",
        component: () => import("pages/goods/PageIncomingGoodsReceiveList.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/incoming/goods/receive/view/:poid/:printed_id",
        name: "Goods Receive",
        component: () => import("pages/goods/PageIncomingGoodsReceiveView.vue"),
        meta: { navigation: "main", save: "true" },
      },
      {
        path: "/create/good/receive/",
        name: "Goods Receive List",
        component: () => import("pages/goods/PageCreateGoodsReceiveList.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/create/good/receive/view/:poid/:printed_id",
        name: "Individual Goods Receive",
        component: () =>
          import("pages/goods/PageIndividualGoodReceiveView.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/internal/transfer/",
        name: "Internal Transfer",
        component: () =>
          import("pages/transfer/Internal/PageInternalTransferList.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/internal/transfer/create/:poid/:printed_id",
        name: "Create Internal Transfer",
        component: () =>
          import("pages/transfer/internal/PageCreateInternalTransfer.vue"),
        meta: {
          navigation: "back",
          save: "true",
        },
      },
      {
        path: "/internal/transfer/create/",
        name: "New Internal Transfer",
        component: () =>
          import(
            "pages/transfer/internal/PageCreateInternalTransferNoData.vue"
          ),
        meta: {
          navigation: "back",
          save: "true",
        },
      },
      {
        path: "/result/internal/transfer/",
        name: "Result Internal Transfer",
        component: () =>
          import("pages/transfer/internal/PageResultInternalTransfer.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "result/internal/transfer/view/:poid/:printed_id",
        name: "Internal Transfer View",
        component: () =>
          import("pages/transfer/internal/PageViewInternalTransfer.vue"),
        meta: {
          navigation: "back",
        },
      },
      {
        path: "/stocktake/",
        name: "Stocktake List",
        component: () => import("pages/stocktake/PageStocktakeList.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/stocktake/details/:stocktakeID",
        name: "Stocktake Details",
        component: () => import("pages/stocktake/PageStocktakeView.vue"),
        meta: { navigation: "back" },
      },
      // {
      //   path: "/stocktake/process/:stocktakeID",
      //   name: "Stocktake",
      //   component: () => import("pages/stocktake/PageStocktakeProcess.vue"),
      //   meta: { navigation: "back", save: "true", reset: "true", tab: true },
      // },
      {
        path: "/stocktake/process/:stocktakeID",
        name: "Stocktake",
        component: () => import("pages/stocktake/PageStocktakeProcessV3.vue"),
        meta: { navigation: "back", save: "true", reset: "true", tab: true },
      },
      // {
      //   path: "/stocktake/process/:stocktakeID",
      //   name: "Stocktake",
      //   component: () => import("pages/stocktake/PageStocktakeProcessV2.vue"),
      //   meta: { navigation: "back", save: "true", reset: "true", tab: true },
      // },
      // {
      //   path: "/stocktake/completed/:stocktakeID",
      //   name: "Stocktaked",
      //   component: () => import("pages/stocktake/PageStocktakeCompleted.vue"),
      //   meta: { navigation: "back", save: "true", reset: "true", tab: true },
      // },
      {
        path: "/stocktake/completed/:stocktakeID",
        name: "Stocktaked",
        component: () => import("pages/stocktake/PageStocktakeCompletedV3.vue"),
        meta: { navigation: "back", save: "true", reset: "true", tab: true },
      },
      {
        path: "/assembly/requests/cards/",
        name: "Assembly Requests Cards",
        component: () =>
          import("pages/assembly/cards/PageAssemblyRequestsCards.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/assembly/requests/cards/create/:poid/:printed_id",
        name: "Create Assembly Requests",
        component: () =>
          import("pages/assembly/cards/PageCreateAssemblyRequests.vue"),
        meta: {
          navigation: "back",
        },
      },
      {
        path: "/account/profile",
        name: "My Account: Update Profile",
        component: () => import("pages/account/PageUpdateProfile.vue"),
        meta: { navigation: "main" },
      },
      {
        path: "/account/password",
        name: "My Account: Change Password",
        component: () => import("pages/account/PageChangePassword.vue"),
        meta: { navigation: "main" },
      },
    ],
  },
  {
    path: "/",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      {
        path: "/login",
        name: "Login",
        component: () => import("pages/login/Index.vue"),
      },
      {
        path: "/login/forgot-password",
        name: "Forgot Password",
        component: () => import("pages/login/PageForgotPassword.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
