import { boot } from "quasar/wrappers";
import CameraButton from "components/Buttons/CameraButton.vue";
import ConfirmationPopup from "src/components/Modals/ConfirmationPopup.vue";
import SubmitButton from "components/Buttons/SubmitButton.vue";
import BackButton from "components/Buttons/BackButton.vue";
import Scanner from "components/WebScanner/scanner.vue";
import LoadingAndNoData from "components/Observer/LoadingAndNoData.vue";
import WebScannerDesign from "components/WebScanner/WebScannerDesign.vue";
import InputDate from "components/Input/InputDate.vue";
import InputSearching from "components/Input/InputSearching.vue";
import lodash from "lodash";
// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  // something to do
  app.component("CameraButton", CameraButton);
  app.component("ConfirmationPopup", ConfirmationPopup);
  app.component("SubmitButton", SubmitButton);
  app.component("BackButton", BackButton);
  app.component("Scanner", Scanner);
  app.component("LoadingAndNoData", LoadingAndNoData);
  app.component("WebScannerDesign", WebScannerDesign);
  app.component("InputDate", InputDate);
  app.component("InputSearching", InputSearching);
  app.use(lodash);
});
