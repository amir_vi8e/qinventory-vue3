import { LocalStorage } from "quasar";
import { api } from "boot/api";

const state = {
  loggedIn: false,
  message: "",
  name: "",
  token: "",
  statusSendLink: 0,
  errorForgotPassword: "",
  messageSendLink: "",
};

const mutations = {
  setAllBlank(state, payload) {
    try {
      Object.assign(state, payload);
    } catch (e) {
      console.log(e);
    }
  },
  setProcess(state, payload) {
    try {
      Object.assign(state, payload);
    } catch (e) {
      console.log(e);
    }
  },
};

const actions = {
  setAllBlank({ commit }) {
    let payload = {
      loggedIn: false,
      message: "",
      name: "",
      token: "",
      statusSendLink: 0,
      errorForgotPassword: "",
      messageSendLink: "",
    };
    commit("setAllBlank", payload);
  },
  loginProcess({ commit }, payload) {
    api({
      method: "post",
      url: "/api/rights/account/login",
      data: payload,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => {
        //handle success
        console.log("Response: ", response);
        if (response.data.success) {
          // handle ok response
          let payload = {
            loggedIn: true,
            message: "",
            name: response.data.name,
            token: response.data.token,
          };

          commit("setProcess", payload);

          if (LocalStorage.getItem("loggedIn")) {
            this.$router.push("/");
          } else {
            LocalStorage.set("loggedIn", true);
            LocalStorage.set("name", response.data.name);
            LocalStorage.set("token", response.data.token);
          }
        } else {
          // handle issue response (invalid email/password, etc)
          let payload = {
            loggedIn: false,
            message: response.data.errors.password[0],
            name: "",
            token: "",
          };

          commit("setProcess", payload);

          LocalStorage.set("loggedIn", false);
        }
      })
      .catch((error) => {
        //handle error
        console.log("error message: ", error.message);
      });
  },
  logoutProcess({ commit }) {
    let payload = {
      loggedIn: false,
      message: "",
      name: "",
      token: "",
    };

    commit("setProcess", payload);

    LocalStorage.set("loggedIn", false);
    LocalStorage.set("name", "");
    LocalStorage.set("token", "");
    this.$router.replace("/login").catch((err) => {});
  },
  sendResetLink({ commit }, payload) {
    api({
      method: "post",
      url: "/api/rights/account/forgot",
      data: payload,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => {
        //handle successful connection
        console.log("response: ", response.data);
        if (response.data.success === 1) {
          // handle successfull send Link action
          let payload = {
            statusSendLink: 1,
            messageSendLink:
              "The Reset Link Has Been Sent To Your Email. Check Your Email Now.",
            errorForgotPassword: "",
          };

          commit("setProcess", payload);
        } else {
          //handle error send link action
          let errorForgotPassword = "";
          if (response.data.error === "") {
            errorForgotPassword =
              "This email address is not registered on the system.";
          } else {
            errorForgotPassword = response.data.error;
          }

          let payload = {
            statusSendLink: response.data.success,
            messageSendLink: "",
            errorForgotPassword: errorForgotPassword,
          };

          commit("setProcess", payload);
        }
      })
      .catch((error) => {
        //handle error connection
        console.log("error message: ", error.message);
      });
  },
};

const getters = {
  token(state) {
    return state.token;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
