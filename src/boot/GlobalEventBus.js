import { boot } from "quasar/wrappers";
// import emitter from "tiny-emitter/instance";
import mitt from "mitt";

const eventBus = mitt();

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  // something to do
  // app.config.globalProperties.$global = {
  //   $on: (...args) => emitter.on(...args),
  //   $once: (...args) => emitter.once(...args),
  //   $off: (...args) => emitter.off(...args),
  //   $emit: (...args) => emitter.emit(...args),
  // };
  app.config.globalProperties.eventBus = eventBus;
});
