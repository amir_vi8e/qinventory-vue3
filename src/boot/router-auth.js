// import router from "src/router";
import { LocalStorage } from "quasar";

export default async ({ router }) => {
  router.beforeEach((to, from, next) => {
    let loggedIn = LocalStorage.getItem("loggedIn");
    let token = LocalStorage.getItem("token");
    if ((token == "" || !loggedIn) && to.path !== "/login") {
      if (to.path === "/login/forgot-password") {
        next();
      } else next("/login");
    } else {
      next();
    }
  });
};
