const state = {
  columns: [
    {
      name: "desc",
      required: true,
      label: "Transfer ID",
      align: "left",
      field: "name",
      format: (val) => `${val}`,
      sortable: true,
      // classes: "bg-grey-2 ellipsis",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "date",
      label: "Transfer Request Date",
      align: "center",
      field: "date",
      sortable: true,
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "from",
      align: "left",
      label: "Transfer Out From",
      field: "from",
      sortable: true,
      filter_type: "select",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "into",
      align: "left",
      label: "Transfer In To",
      field: "into",
      sortable: true,
      filter_type: "select",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "request",
      align: "left",
      label: "Request By",
      field: "request",
      sortable: true,
      filter_type: "select",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "status",
      align: "center",
      label: "Status",
      field: "status",
      sortable: true,
      filter_type: "select",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "actions",
      align: "center",
      label: "Action(s)",
      field: "actions",
      headerClasses: "bg-accent text-grey-11",
    },
  ],
  data: [
    {
      name: "TRN21050452",
      date: "11 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Assembly Disassembly",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050478",
      date: "11 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Kimberly Kim",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN30550452",
      date: "18 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Assembly Disassembly",
      status: "draft",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050425",
      date: "20 May 2021",
      from: "Main Warehouse (07-28)",
      into: "1010 - City Square Mall",
      request: "Assembly Disassembly",
      status: "pending",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050424",
      date: "24 May 2021",
      from: "Main Warehouse (07-28)",
      into: "1010 - City Square Mall",
      request: "Kimberly Kim",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050458",
      date: "28 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Assembly Disassembly",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050989",
      date: "30 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Assembly Disassembly",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050880",
      date: "30 May 2021",
      from: "Main Warehouse (07-28)",
      into: "1010 - City Square Mall",
      request: "Kimberly Kim",
      status: "approved",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21050690",
      date: "31 May 2021",
      from: "1010 - City Square Mall",
      into: "Main Warehouse (07-28)",
      request: "Assembly Disassembly",
      status: "pending",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
    {
      name: "TRN21057998",
      date: "1 June 2021",
      from: "Main Warehouse (07-28)",
      into: "1010 - City Square Mall",
      request: "Kimberly Kim",
      status: "rejected",
      stockid: "",
      stockremarks: "",
      trremarks: "",
      products: [
        {
          no: 1,
          pcode: "OAG001",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
        {
          no: 2,
          pcode: "OAG002",
          pname: "Sesame Seeds Grade A",
          qtyfrom: "31000.243 kg",
          qtylocation: "10000 kg",
          buffer: "kg",
          qtytransferred: "100000 kg",
        },
      ],
    },
  ],
};
const mutations = {
  changeApproved(state, payload) {
    try {
      if (state.data.find((o) => o.name === payload)) {
        let index = state.data.findIndex((o) => o.name === payload);
        state.data[index].status = "approved";
      }
    } catch (error) {
      console.log(error);
    }
  },
  changeRejected(state, payload) {
    try {
      if (state.data.find((o) => o.name === payload)) {
        let index = state.data.findIndex((o) => o.name === payload);
        state.data[index].status = "rejected";
      }
    } catch (error) {
      console.log(error);
    }
  },
};

const actions = {
  changeApproved({ commit }, data) {
    data.forEach((item) => {
      let payload = item;
      commit("changeApproved", payload);
    });
  },
  changeRejected({ commit }, data) {
    data.forEach((item) => {
      let payload = item;
      commit("changeRejected", payload);
    });
  },
};
const getters = {
  columns: (state) => {
    return state.columns;
  },
  data: (state) => {
    return state.data;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
