import Vue from "vue";
import { uid } from "quasar";

const state = {
  columns: [
    {
      name: "pcode",
      label: "Product Code",
      align: "center",
      field: "pcode",
      sortable: true,
      headerClasses: "bg-accent",
    },
    {
      name: "product_name",
      label: "Product Name",
      field: "pname",
      align: "center",
      sortable: true,
      classes: "text-left",
      headerClasses: "bg-accent",
    },
    {
      name: "qty_from_source",
      align: "center",
      label: "Qty From Source",
      field: "qtysource",
      headerClasses: "bg-accent",
    },
    {
      name: "qty_from_destination",
      align: "center",
      label: "Qty From Destination",
      field: "qtydestination",
      headerClasses: "bg-accent",
    },
    {
      name: "qty",
      label: "Qty",
      field: "qty",
      align: "center",
      sortable: true,
      sort: (a, b) => parseInt(a, 10) - parseInt(b, 10),
      headerClasses: "bg-accent",
    },
    {
      name: "actions",
      align: "center",
      label: "Action(s)",
      field: "actions",
      headerClasses: "bg-accent",
    },
  ],
  data: [
    {
      name: "MPEN",
      pcode: "MPEN",
      brand: "505",
      pname: "Belly Pork",
      attribute: "",
      ptype: "simple",
      category: "Tourist - Merlion Pen",
      status: "Enabled",
      imonitor: "Yes",
      istatus: "Out of Stock",
      uprice: "$12.06",
      soh: "0 PVC can",
      qty: 1,
      qtysource: 0,
      qtydestination: 0,
      customer: "Rob",
      outlet: "Main Warehouse",
      delete: true,
      products: {
        columns: [
          {
            name: "name",
            label: "Name",
            align: "center",
            field: "name",
            sortable: true,
            classes: "text-left",
            headerClasses: "bg-grey-2",
          },
          {
            name: "pcode",
            label: "Product Code",
            field: "pcode",
            align: "center",
            sortable: true,
            classes: "text-left",
            headerClasses: "bg-grey-2",
          },
          {
            name: "type",
            align: "center",
            label: "Inventory Type",
            field: "type",
            classes: "text-left",
            headerClasses: "bg-grey-2",
          },
          {
            name: "available",
            align: "center",
            label: "Qty Available",
            field: "available",
            headerClasses: "bg-grey-2",
          },
          {
            name: "required",
            label: "Required",
            field: "required",
            align: "center",
            sortable: true,
            sort: (a, b) => parseInt(a, 10) - parseInt(b, 10),
            headerClasses: "bg-grey-2",
          },
        ],
        data: [
          {
            name: "Pork Collar Dalia",
            pcode: "SF-OPP-B",
            type: "Finished Good",
            available: "0 pcs",
            required: "1 pcs",
          },
          {
            name: "Pork Belly Animex",
            pcode: "SF-OPP-A",
            type: "Finished Good",
            available: "0 pcs",
            required: "1 pcs",
          },
        ],
      },
    },
  ],
  columns2: [
    {
      name: "sku",
      label: "Product Code",
      align: "center",
      field: "name",
      sortable: true,
      // classes: "bg-grey-2 ellipsis",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "product_name",
      label: "Product Name",
      field: "pname",
      align: "center",
      sortable: true,
      classes: "text-left",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "qty_from_source",
      align: "center",
      label: "Qty From Source",
      field: "qtysource",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "qty_from_destination",
      align: "center",
      label: "Qty From Destination",
      field: "qtydestination",
      headerClasses: "bg-accent text-primary",
    },
    {
      name: "qty",
      label: "Qty",
      field: "qty",
      align: "center",
      sortable: true,
      sort: (a, b) => parseInt(a, 10) - parseInt(b, 10),
      headerClasses: "bg-accent text-primary",
    },
  ],
};

const mutations = {
  addData(state, payload) {
    console.log(payload);
    try {
      //   Vue.set(state.data, payload.id, payload.data);
      console.log(payload.data.name);
      if (state.data.find((o) => o.name === payload.data.name)) {
        let index = state.data.findIndex((o) => o.name === payload.data.name);
        let qty = parseInt(state.data[index].qty) + parseInt(payload.data.qty);

        console.log("HERE!");
        console.log(state.data[index].qty);
        console.log(payload.data.qty);
        console.log(qty);

        state.data[index].qty = qty;
        console.log(state.data[index].qty);
        console.log("END!");
      } else {
        state.data.push(payload.data);
      }
      console.log("success");
    } catch (error) {
      console.log("try catch");
      console.log(error);
    }
  },
  deleteData(state, payload) {
    try {
      if (state.data.find((o) => o.name === payload)) {
        let index = state.data.findIndex((o) => o.name === payload);
        console.log(index);
        state.data.splice(index, 1);
      }
    } catch (error) {
      console.log(error);
    }
  },
};

const actions = {
  addData({ commit }, data) {
    Object.entries(data).forEach((entry) => {
      console.log(entry[1]);
      let dataId = uid();
      let payload = {
        id: dataId,
        data: entry[1],
      };
      commit("addData", payload);
    });
  },
  deleteData({ commit }, data) {
    let payload = data;
    console.log(data);
    commit("deleteData", payload);
  },
};

const getters = {
  columns: (state) => {
    return state.columns;
  },
  data: (state) => {
    return state.data;
  },
  columns2: (state) => {
    return state.columns2;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
